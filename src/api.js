// const serverurl = 'http://localhost:8181/api/'
const serverurl = 'api/'
export const api = {
  // PUBLIC
  async search (name, tags, param, sort, size, page) {
    var url = serverurl + 'search'
    const params = new URLSearchParams()

    if (name != null) params.append('name', name)
    if (tags != null) params.append('tags', tags)
    if (param != null) params.append('param', param)
    if (sort != null) params.append('sort', sort)

    params.append('size', size)
    params.append('page', page)

    console.log('get models ' + this.paramSearch + ' + ' + this.paramPerf + ' + ' + this.paramTags)

    url += '?' + params
    const response = await fetch(url)
    try {
      return await response.json()
    } catch (error) {
      return null
    }
  },
  async getTags () {
    const url = serverurl + 'tags'
    const response = await fetch(url)
    try {
      return await response.json()
    } catch (error) {
      return null
    }
  },
  async getModel (id) {
    const url = serverurl + 'models' + '?id=' + id
    const response = await fetch(url)
    try {
      return await response.json()
    } catch (error) {
      return null
    }
  },
  async login (username, password) {
    const url = serverurl + 'users/login'
    try {
      const response = await fetch(
        url, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            username: username,
            password: password
          })
        }
      )
      return await response.json()
    } catch (error) {
      return null
    }
  },
  async register (username, email, password) {
    const url = serverurl + 'users/signup'
    try {
      const response = await fetch(
        url, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            email: email,
            username: username,
            password: password
          })
        }
      )
      return await response.json()
    } catch (error) {
      return null
    }
  },
  async getModelComments (id) {
    const url = serverurl + 'comments' + '?id=' + id
    const response = await fetch(url)
    try {
      return await response.json()
    } catch (error) {
      return null
    }
  },
  async download (id, type, name) {
    // c'est un peu hacky
    // on créé un 'a' invisble avec un attribut 'download'
    // le problème, c'est de créé un attribut dynamique
    console.log('Download ' + id + ' | ' + name + ' | ' + type)
    const url = serverurl + type + '/download?id=' + id

    fetch(url)
      .then(response => response.blob())
      .then(blob => {
        const url = window.URL.createObjectURL(blob)
        const a = document.createElement('a')
        a.style.display = 'none'
        a.href = url
        a.download = name
        document.body.appendChild(a)
        a.click()
        window.URL.revokeObjectURL(url)
      })
      .catch(() => this.$buefy.toast.open('Download error'))
  },
  // USER
  async getUser () {
    const token = await localStorage.getItem('token')
    const url = serverurl + 'users'
    try {
      const response = await fetch(url, { headers: { Authorization: 'Bearer ' + token } })
      return await response.json()
    } catch (error) {
      return null
    }
  },
  async putUser (account) {
    const token = await localStorage.getItem('token')
    const url = serverurl + 'users'
    try {
      const response = await fetch(url, {
        method: 'PUT',
        headers: {
          Authorization: 'Bearer ' + token,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          email: account.email,
          username: account.username,
          password: account.password,
          id: account.id
        })
      })
      return await response.json()
    } catch (error) {
      console.log(error)
      return null
    }
  },
  async getUserModels () {
    const token = await localStorage.getItem('token')
    const url = serverurl + 'models'
    try {
      const response = await fetch(url, { headers: { Authorization: 'Bearer ' + token } })
      return await response.json()
    } catch (error) {
      return null
    }
  },
  async getUserLikedModels () {
    const token = await localStorage.getItem('token')
    const url = serverurl + 'vote/likedModels'
    try {
      const response = await fetch(url, { headers: { Authorization: 'Bearer ' + token } })
      return await response.json()
    } catch (error) {
      return null
    }
  },
  async addNewTag (tag, categoryId) {
    const url = serverurl + 'tags'
    const token = await localStorage.getItem('token')
    try {
      const response = await fetch(
        url, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + token
          },
          body: JSON.stringify({
            name: tag,
            categoryId: categoryId
          })
        }
      )
      return await response.json()
    } catch (error) {
      return null
    }
  },
  async postModel (model) {
    const url = serverurl + 'models'
    const token = await localStorage.getItem('token')
    try {
      const response = await fetch(url, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token
        },
        body: JSON.stringify({
          name: model.name,
          shortDescription: model.shortDescription,
          longDescription: model.longDescription,
          performance: model.performance,
          performanceUnit: model.performanceUnit,
          parameterCount: model.parameterCount,
          tags: model.tags,
          performanceLowerIsBetter: model.performanceLowerIsBetter
        })
      })
      return await response.json()
    } catch (error) {
      return null
    }
  },
  async putModel (id, model) {
    const url = serverurl + 'models' + '?id=' + id
    const token = await localStorage.getItem('token')
    const response = await fetch(url, {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token
      },
      body: JSON.stringify({
        name: model.name,
        shortDescription: model.shortDescription,
        longDescription: model.longDescription,
        performance: model.performance,
        performanceUnit: model.performanceUnit,
        parameterCount: model.parameterCount,
        tags: model.tags,
        performanceLowerIsBetter: model.performanceLowerIsBetter
      })
    })
    try {
      return await response.json()
    } catch (error) {
      return null
    }
  },
  async uploadModelFile (id, file) {
    const data = new FormData()
    const url = serverurl + 'models/upload'
    const token = await localStorage.getItem('token')

    data.append('file', file)
    data.append('id', id)

    try {
      const response = await fetch(url, {
        method: 'POST',
        headers: {
          Authorization: 'Bearer ' + token
        },
        body: data
      })
      if (response.status !== 200) return false
      return true
    } catch (error) {
      return false
    }
  },
  async uploadLayers (id, customLayers) {
    const token = await localStorage.getItem('token')
    for (var i = 0; i < customLayers.length; i++) {
      const layer = customLayers[i]
      const data = new FormData()
      const url = serverurl + 'layers/upload'

      console.log('Uploading layer ' + i + ' | ' + layer.file + ' to ' + id)
      if (layer.file === undefined) continue
      if (layer.name === undefined) continue

      data.append('file', layer.file)
      data.append('name', layer.name)
      data.append('id', id)

      try {
        const response = await fetch(url, {
          method: 'POST',
          headers: {
            Authorization: 'Bearer ' + token
          },
          body: data
        })
        if (response.status !== 200) return false
      } catch (error) {
        return false
      }
    }
    return true
  },
  async removeModel (id) {
    const token = await localStorage.getItem('token')
    const url = serverurl + 'models' + '?id=' + id
    try {
      await fetch(url, {
        method: 'DELETE',
        headers: {
          Authorization: 'Bearer ' + token
        }
      })
      return true
    } catch (error) {
      return false
    }
  },
  async getVoteStatus (id) {
    const url = serverurl + 'vote' + '?id=' + id
    const token = await localStorage.getItem('token')
    const response = await fetch(url, {
      headers: {
        Authorization: 'Bearer ' + token
      }
    })
    try {
      return await response.json()
    } catch (error) {
      return null
    }
  },
  async addVote (id, isDown) {
    const url = serverurl + 'vote' + '?id=' + id + '&isDown=' + isDown
    const token = await localStorage.getItem('token')
    const response = await fetch(url, {
      method: 'PUT',
      headers: {
        Authorization: 'Bearer ' + token
      }
    })
    try {
      return await response.json()
    } catch (error) {
      return null
    }
  },
  async removeVote (id, isDown) {
    const url = serverurl + 'vote' + '?id=' + id + '&isDown=' + isDown
    const token = await localStorage.getItem('token')
    const response = await fetch(url, {
      method: 'DELETE',
      headers: {
        Authorization: 'Bearer ' + token
      }
    })
    try {
      return await response.json()
    } catch (error) {
      return null
    }
  },
  async addComment (id, content) {
    const url = serverurl + 'comments'
    const token = await localStorage.getItem('token')
    try {
      const response = await fetch(
        url, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + token
          },
          body: JSON.stringify({
            modelId: id,
            content: content
          })
        }
      )
      return await response.json()
    } catch (error) {
      return null
    }
  },
  async removeComment (id) {
    const token = await localStorage.getItem('token')
    const url = serverurl + 'comments' + '?id=' + id
    try {
      await fetch(url, {
        method: 'DELETE',
        headers: {
          Authorization: 'Bearer ' + token
        }
      })
      return true
    } catch (error) {
      return false
    }
  },
  // ADMIN
  async addNewCategory (name) {
    const url = serverurl + 'tags/category'
    const token = await localStorage.getItem('token')
    try {
      const response = await fetch(
        url, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + token
          },
          body: JSON.stringify({
            name: name
          })
        }
      )
      return await response.json()
    } catch (error) {
      return null
    }
  },
  async getUserList () {
    const token = await localStorage.getItem('token')
    const url = serverurl + 'users/list'
    try {
      const response = await fetch(url, { headers: { Authorization: 'Bearer ' + token } })
      return await response.json()
    } catch (error) {
      return null
    }
  },
  async getModelList () {
    const token = await localStorage.getItem('token')
    const url = serverurl + 'models/list'
    try {
      const response = await fetch(url, { headers: { Authorization: 'Bearer ' + token } })
      return await response.json()
    } catch (error) {
      return null
    }
  },
  async removeTag (id) {
    const token = await localStorage.getItem('token')
    const url = serverurl + 'tags' + '?id=' + id
    try {
      await fetch(url, {
        method: 'DELETE',
        headers: {
          Authorization: 'Bearer ' + token
        }
      })
      return true
    } catch (error) {
      return false
    }
  },
  async removeCategory (id) {
    const token = await localStorage.getItem('token')
    const url = serverurl + 'tags/category' + '?id=' + id
    try {
      await fetch(url, {
        method: 'DELETE',
        headers: {
          Authorization: 'Bearer ' + token
        }
      })
      return true
    } catch (error) {
      return false
    }
  },
  async removeUser (id) {
    const token = await localStorage.getItem('token')
    const url = serverurl + 'users' + '?id=' + id
    try {
      await fetch(url, {
        method: 'DELETE',
        headers: {
          Authorization: 'Bearer ' + token
        }
      })
      return true
    } catch (error) {
      return false
    }
  },
  async getCommentList (id) {
    const token = await localStorage.getItem('token')
    const url = serverurl + 'comments/list'
    const response = await fetch(url, {
      headers: {
        Authorization: 'Bearer ' + token
      }
    })
    try {
      return await response.json()
    } catch (error) {
      return null
    }
  },
  async setVerified (id) {
    const url = serverurl + 'models/setVerified' + '?id=' + id
    const token = await localStorage.getItem('token')
    const response = await fetch(url, {
      headers: {
        Authorization: 'Bearer ' + token
      }
    })
    try {
      return await response.json()
    } catch (error) {
      return null
    }
  },
  async setAdmin (id) {
    const url = serverurl + 'users/setAdmin' + '?id=' + id
    const token = await localStorage.getItem('token')
    const response = await fetch(url, {
      headers: {
        Authorization: 'Bearer ' + token
      }
    })
    try {
      return await response.json()
    } catch (error) {
      return null
    }
  }
}
